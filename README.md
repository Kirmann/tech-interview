########################################
Technical interview assignment:
########################################
Part of our everyday responsibilities as developers is to empower each other with knowledge.

You are given 45 minutes to review code provided by another developer for the task he had.

You can ask questions during the task. You are free to google and run the application.

Write down your findings and ideas how to help this developer succeed better in the future.

PS: Data objects are validated according to business rules and developer talked with stakeholder about them.
PS2: Developer also has tested the application and everything is working in his machine.

######################################
Developer had a following task:
######################################

1. Make a Spring Boot application (java 1.8)

1.1 Create 3 users into the database (username and password)

1.1 User has to log in with one of the users

1.2 User is displayed list of loans that he has entered. User is not allowed to see other user loans.

1.3 User can add and edit loans

1.4 Store all input data to database

1.5 Values of country select box has to be populated with data from database.

1.6 ORM usage is fine, if its not hibernate.


######################################
Developer comments
######################################
3 users to login with:

 mickey, santa, donald
 
all having same password 123